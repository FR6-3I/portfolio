FROM node:20.9.0-alpine AS base

# Dependencies
FROM base AS dependencies
WORKDIR /app
VOLUME [ "/pnpm-store", "/app/node_modules" ]

RUN corepack enable && pnpm config --global set store-dir /pnpm-store

COPY package.json pnpm-lock.yaml ./

RUN pnpm install


# Builder
FROM base AS builder
WORKDIR /app
ENV NEXT_TELEMETRY_DISABLED 1

COPY --from=dependencies /app/node_modules ./node_modules
COPY . .

RUN corepack enable && pnpm build


# Runner
FROM base AS runner
WORKDIR /app
ENV PORT 3000
ENV HOSTNAME "0.0.0.0"
ENV NEXT_TELEMETRY_DISABLED 1

RUN addgroup --system --gid 1001 nodejs && adduser --system --uid 1001 nextjs

COPY --from=builder /app/public ./public

RUN mkdir .next && chown nextjs:nodejs .next

COPY --from=builder --chown=nextjs:nodejs /app/.next/standalone ./
COPY --from=builder --chown=nextjs:nodejs /app/.next/static ./.next/static

USER nextjs

EXPOSE 3000

CMD ["node", "server.js"]

# podman build -t portfolio -v "$HOME/.local/share/pnpm/store:/pnpm-store" -v "$(pwd)/node_modules:/app/node_modules" .
# podman run -p 3000:3000 portfolio
