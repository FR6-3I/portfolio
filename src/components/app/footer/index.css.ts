import { style } from '@vanilla-extract/css'

import { BP_SM, theme } from '~/theme/theme.css'

export const root = style({
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    width: '100%',
    marginTop: 'auto',
    backgroundColor: theme.palette.background.secondary,
    '@media': {
        [BP_SM]: { flexDirection: 'initial' },
    },
})
