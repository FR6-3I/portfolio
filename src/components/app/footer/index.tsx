import type { JSX } from 'react'

import CardFooter from './CardFooter'

import { links } from '~/utils/constants'

import type { LinkFooter } from '~/types/general'
import * as styles from './index.css'

const Footer = (): JSX.Element => {
    const linksProduction: LinkFooter[] = [
        {
            name: 'Portfolio',
            link: links.online.PORTFOLIO,
        },
        {
            name: 'Poké-Infos',
            link: links.online.POKE_INFOS,
        },
    ]

    const linksCode: LinkFooter[] = [
        {
            name: 'Portfolio',
            link: links.code.PORTFOLIO,
        },
        {
            name: 'Poké-Infos',
            link: links.code.POKE_INFOS,
        },
    ]

    const linksProfiles: LinkFooter[] = [
        {
            name: 'GitLab',
            link: links.profile.GITLAB,
        },
        {
            name: 'LinkedIn',
            link: links.profile.LINKEDIN,
        },
    ]

    return (
        <footer className={styles.root}>
            <CardFooter
                title='En ligne'
                links={linksProduction}
            />
            <CardFooter
                title='Code source'
                links={linksCode}
            />
            <CardFooter
                title='Réseaux'
                links={linksProfiles}
            />
        </footer>
    )
}

export default Footer
