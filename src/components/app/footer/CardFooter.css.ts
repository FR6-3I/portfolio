import { style } from '@vanilla-extract/css'

import { BP_SM, theme } from '~/theme/theme.css'

export const title = style({
    marginBottom: theme.margin,
    fontSize: '1.2em',
    fontWeight: 'bold',
    textAlign: 'center',
})

export const content = style({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginLeft: '20%',
    '@media': {
        [BP_SM]: { marginLeft: 'initial' },
    },
})

export const link = style({
    margin: '5px 0px',
})
