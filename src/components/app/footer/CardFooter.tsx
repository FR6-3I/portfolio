import type { JSX } from 'react'

import { Card, Link } from '~/ui'

import type { LinkFooter } from '~/types/general'
import * as styles from './CardFooter.css'

type CardFooterProperties = {
    title: string
    links: LinkFooter[]
}

const CardFooter = ({
    title,
    links,
}: Readonly<CardFooterProperties>): JSX.Element => {
    return (
        <Card
            variant='pressed'
            color='secondary'
        >
            <div className={styles.title}>{title}</div>

            <div className={styles.content}>
                {links.map((link) => (
                    <Link
                        key={link.name}
                        className={styles.link}
                        href={link.link}
                    >
                        {link.name}
                    </Link>
                ))}
            </div>
        </Card>
    )
}

export default CardFooter
