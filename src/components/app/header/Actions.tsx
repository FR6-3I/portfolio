import type { JSX } from 'react'
import { useEffect, useState } from 'react'
import { useTheme } from 'next-themes'
import { FaGitlab, FaLinkedin } from 'react-icons/fa'
import { MdBrightness3, MdWbSunny } from 'react-icons/md'

import { Button, Link, Tooltip } from '~/ui'

import { links } from '~/utils/constants'

import { Theme } from '~/types/general'
import * as styles from './Actions.css'

const Actions = (): JSX.Element | null => {
    const { theme, setTheme, systemTheme } = useTheme()

    const [mounted, setMounted] = useState(false)
    const [themeUsed, setThemeUsed] = useState<Theme>(Theme.enum.dark)

    /** Update the theme. */
    const updateTheme = (): void => {
        if (themeUsed === Theme.enum.dark) {
            setTheme(Theme.enum.light)

            return
        }

        setTheme(Theme.enum.dark)
    }

    // Update the theme used
    useEffect(() => {
        if (!theme || theme === 'system') {
            if (systemTheme === Theme.enum.dark) setThemeUsed(Theme.enum.dark)

            setThemeUsed(Theme.enum.light)
        }

        if (theme && theme !== 'system') setThemeUsed(theme as Theme)
    }, [theme, systemTheme])

    // Prevent flashing theme logo
    useEffect(() => {
        setMounted(true)
    }, [])

    /* eslint-disable-next-line unicorn/no-null */
    if (!mounted) return null

    return (
        <div className={styles.root}>
            {/* GitLab profile button */}
            <Tooltip text='Profil GitLab'>
                <Link
                    variant='button'
                    color='secondary'
                    href={links.profile.GITLAB}
                    aria-label='Profil GitLab'
                >
                    <FaGitlab
                        size='1.2em'
                        role='presentation'
                    />

                    <div className={styles.textButton}>{'Profil GitLab'}</div>
                </Link>
            </Tooltip>

            {/* LinkedIn profile button */}
            <Tooltip text='Profil LinkedIn'>
                <Link
                    variant='button'
                    color='secondary'
                    href={links.profile.LINKEDIN}
                    aria-label='Profil LinkedIn'
                >
                    <FaLinkedin
                        size='1.2em'
                        role='presentation'
                    />

                    <div className={styles.textButton}>{'Profil LinkedIn'}</div>
                </Link>
            </Tooltip>

            {/* Theme button */}
            <Tooltip text='Changer de thème'>
                <Button
                    onClick={updateTheme}
                    color='secondary'
                >
                    {themeUsed === Theme.enum.dark && (
                        <MdWbSunny
                            size='1.2em'
                            role='img'
                            aria-label='Clair'
                        />
                    )}

                    {themeUsed === Theme.enum.light && (
                        <MdBrightness3
                            size='1.2em'
                            role='img'
                            aria-label='Sombre'
                        />
                    )}

                    <div className={styles.textButton}>
                        {'Changer de thème'}
                    </div>
                </Button>
            </Tooltip>
        </div>
    )
}

export default Actions
