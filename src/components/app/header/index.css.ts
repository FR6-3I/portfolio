import { globalStyle, style } from '@vanilla-extract/css'

import { BP_LG, BP_MD, BP_SM, BP_XL, theme } from '~/theme/theme.css'

export const root = style({
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    height: '12vh',
    padding: '0 2vw',
    backgroundColor: theme.palette.background.secondary,
    '@media': {
        [BP_MD]: { padding: '0 5vw' },
        [BP_LG]: { padding: '0 10vw' },
        [BP_XL]: { padding: '0 15vw' },
    },
})

export const textHomepage = style({
    display: 'none',
    marginLeft: '5px',
    fontSize: '1.2em',
    fontWeight: 'bold',
    '@media': {
        [BP_XL]: { display: 'inherit' },
    },
})

export const input = style({
    marginLeft: 'auto',
    '@media': {
        [BP_SM]: { display: 'none' },
    },
})

export const navigation = style({
    position: 'absolute',
    top: '12vh',
    left: '0px',
    display: 'none',
    width: '100%',
    height: '100%',
    backgroundColor: theme.palette.background.secondary,
    borderTop: `2px solid ${theme.palette.background.primary}`,
    '@media': {
        [BP_SM]: {
            position: 'inherit',
            display: 'flex',
            borderTop: 'inherit',
        },
    },
})

// Header sticky
globalStyle(`${root}:has(${input} > input:checked)`, {
    position: 'fixed',
    '@media': {
        [BP_SM]: { position: 'inherit' },
    },
})

// Affiche les choix en sticky
globalStyle(`${input}:has(> input:checked) + ${navigation}`, {
    position: 'fixed',
    display: 'block',
    '@media': {
        [BP_SM]: {
            position: 'inherit',
            display: 'inherit',
        },
    },
})

// Affiche en colonne
globalStyle(`${navigation} > div`, {
    flexDirection: 'column',
    '@media': {
        [BP_SM]: { flexDirection: 'inherit' },
    },
})

// Tous les width large
globalStyle(
    `
${navigation} > div > div,
${navigation} > div > div > a
`,
    {
        display: 'flex',
        width: '100%',
    },
)

// Alignement du contenu
globalStyle(
    `
${navigation} > div > div > a,
${navigation} > div > div > button
`,
    {
        justifyContent: 'left',
        paddingLeft: '15%',
        '@media': {
            [BP_SM]: {
                paddingLeft: '20px',
            },
        },
    },
)
