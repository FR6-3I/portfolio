import type { JSX } from 'react'
import { IoFileTrayFullSharp } from 'react-icons/io5'

import { Link, Tooltip } from '~/ui'

import { urls } from '~/utils/constants'

import type { UrlHeader } from '~/types/general'
import * as styles from './Pages.css'

const Navigation = (): JSX.Element => {
    const urlsHeader: UrlHeader[] = [
        {
            name: 'Projets',
            href: urls.PROJECTS,
            tooltip: 'Parcourir mes projets 🤠',
            icon: (
                <IoFileTrayFullSharp
                    size='1.2em'
                    role='presentation'
                />
            ),
        },
    ]

    return (
        <div className={styles.root}>
            {urlsHeader.map((url) => (
                <Tooltip
                    key={url.name}
                    text={url.tooltip}
                >
                    <Link
                        variant='button'
                        color='secondary'
                        href={url.href}
                    >
                        {url.icon}

                        <div className={styles.textButton}>{url.name}</div>
                    </Link>
                </Tooltip>
            ))}
        </div>
    )
}

export default Navigation
