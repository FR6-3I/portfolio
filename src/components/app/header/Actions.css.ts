import { globalStyle, style } from '@vanilla-extract/css'

import { secondary } from '~/ui/Button/index.css'

import { BP_MD, BP_SM } from '~/theme/theme.css'

export const root = style({
    display: 'flex',
    margin: 'auto 0px',
})

export const textButton = style({
    marginLeft: '10px',
    fontSize: 'inherit',
    '@media': {
        [BP_SM]: { display: 'none' },
        [BP_MD]: { display: '' },
    },
})

globalStyle(`${root} > div > ${secondary}`, {
    width: '100%',
    '@media': {
        [BP_SM]: {
            width: 'auto',
            margin: 'auto 10px',
        },
    },
})
