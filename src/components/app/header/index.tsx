import type { JSX } from 'react'
import Image from 'next/image'
import { MdMenu } from 'react-icons/md'

import { Checkbox, Link } from '~/ui'
import Actions from './Actions'
import Pages from './Pages'

import { urls } from '~/utils/constants'

import logo from '~/../public/logo.svg'
import * as styles from './index.css'

const Header = (): JSX.Element => {
    return (
        <header className={styles.root}>
            {/* Homepage button */}
            <Link
                variant='button'
                color='secondary'
                href={urls.HOMEPAGE}
            >
                <Image
                    src={logo as string}
                    alt='Logo'
                    width={35}
                    height={35}
                    quality={100}
                    role='img'
                    priority
                />

                <div className={styles.textHomepage}>{'Portfolio'}</div>
            </Link>

            {/* Show choices button in small screens */}
            <Checkbox
                className={styles.input}
                variant='button'
                color='secondary'
                aria-label='Menu'
            >
                <MdMenu
                    fontSize='1.2em'
                    role='presentation'
                />
            </Checkbox>

            {/* Choices available */}
            <nav className={styles.navigation}>
                <Pages />
                <Actions />
            </nav>
        </header>
    )
}

export default Header
