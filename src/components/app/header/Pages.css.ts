import { globalStyle, style } from '@vanilla-extract/css'

import { secondary } from '~/ui/Button/index.css'

import { BP_MD, BP_SM, theme } from '~/theme/theme.css'

export const root = style({
    display: 'flex',
    margin: 'auto',
})

export const textButton = style({
    marginLeft: '10px',
    '@media': {
        [BP_SM]: { display: 'none' },
        [BP_MD]: { display: 'inherit' },
    },
})

globalStyle(`${root} > div > ${secondary}`, {
    '@media': {
        [BP_SM]: {
            margin: `auto ${theme.margin}`,
        },
    },
})
