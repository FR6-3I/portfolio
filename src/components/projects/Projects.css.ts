import { style } from '@vanilla-extract/css'

import { BP_2XL, BP_LG, BP_MD, BP_SM, BP_XL } from '~/theme/theme.css'

export const root = style({
    marginBottom: '5vh',
})

export const description = style({
    marginBottom: '5vh',
    textAlign: 'justify',
    textIndent: '50px',
})

export const buttonsGroup = style({
    display: 'grid',
    justifyContent: 'center',
    '@media': {
        [BP_SM]: { gridTemplateColumns: 'repeat(2, 50%)' },
        [BP_MD]: { gridTemplateColumns: 'repeat(2, 45%)' },
        [BP_LG]: { gridTemplateColumns: 'repeat(2, 40%)' },
        [BP_XL]: { gridTemplateColumns: 'repeat(2, 36%)' },
        [BP_2XL]: { gridTemplateColumns: 'repeat(2, 30%)' },
    },
})

export const textButton = style({
    marginLeft: '10px',
})
