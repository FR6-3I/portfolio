import type { JSX } from 'react'
import { FaGitlab } from 'react-icons/fa'
import { MdWeb } from 'react-icons/md'

import { Card, Heading, Link } from '~/ui'

import type { Project } from '~/types/general'
import * as styles from './Projects.css'

type ProjectsProperties = {
    projects: Project[]
}

export const Projects = ({
    projects,
}: Readonly<ProjectsProperties>): JSX.Element => {
    return (
        <div className={styles.root}>
            <Heading>{'📚 Projets'}</Heading>

            {projects.map((project) => (
                <Card
                    key={project.name}
                    variant='pressed'
                >
                    {/* Nom du projet */}
                    <Heading variant='h2'>{project.name}</Heading>

                    {/* Description */}
                    <div className={styles.description}>
                        {project.description}
                    </div>

                    {/* Boutons En ligne et Code source */}
                    <div className={styles.buttonsGroup}>
                        {/* En ligne */}
                        <Link
                            variant='button'
                            href={project.linkOnline}
                        >
                            <MdWeb
                                size='1.5em'
                                role='presentation'
                            />

                            <div className={styles.textButton}>
                                {'En ligne'}
                            </div>
                        </Link>

                        {/* Code source */}
                        <Link
                            variant='button'
                            href={project.linkCode}
                        >
                            <FaGitlab
                                size='1.5em'
                                role='presentation'
                            />

                            <div className={styles.textButton}>
                                {'Code source'}
                            </div>
                        </Link>
                    </div>
                </Card>
            ))}
        </div>
    )
}
