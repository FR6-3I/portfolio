import type { JSX } from 'react'

import { Card, Heading } from '~/ui'

import * as styles from './Introduction.css'

export const Introduction = (): JSX.Element => {
    return (
        <div className={styles.root}>
            <Heading>{'📖 Introduction'}</Heading>

            <Card
                className={styles.text}
                variant='pressed'
            >
                {`Tous les projets (y compris ce portfolio) sont adaptables selon l’écran (responsive) 📱/🖥️.
        Un lien pour accéder directement au projet en ligne ainsi qu’au code source sont mis à disposition.
        Je vous souhaite une bonne découverte ! 🤠`}
            </Card>
        </div>
    )
}
