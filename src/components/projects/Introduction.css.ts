import { style } from '@vanilla-extract/css'

export const root = style({
    marginBottom: '5vh',
})

export const text = style({
    marginBottom: '5vh',
    textAlign: 'justify',
    textIndent: '50px',
})
