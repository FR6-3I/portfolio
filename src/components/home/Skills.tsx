import type { JSX } from 'react'
import {
    SiFastify,
    SiGraphql,
    SiJavascript,
    SiJest,
    SiMui,
    SiNextdotjs,
    SiNodedotjs,
    SiPlaywright,
    SiPrisma,
    SiReact,
    SiRust,
    SiTestinglibrary,
    SiTypescript,
} from 'react-icons/si'

import { Card, Heading, List, ListItem } from '~/ui'

import type { Skill } from '~/types/general'
import * as styles from './Skills.css'

export const Skills = (): JSX.Element => {
    const skills: Skill[] = [
        // Languages
        {
            name: 'Langages',
            technologies: [
                {
                    name: 'JavaScript',
                    icon: (
                        <SiJavascript
                            size='1.4em'
                            color='orange'
                            role='presentation'
                        />
                    ),
                },
                {
                    name: 'TypeScript',
                    icon: (
                        <SiTypescript
                            size='1.4em'
                            color='dodgerBlue'
                            role='presentation'
                        />
                    ),
                },
                {
                    name: 'Rust',
                    icon: (
                        <SiRust
                            size='1.4em'
                            color='saddleBrown'
                            role='presentation'
                        />
                    ),
                },
                {
                    name: 'GraphQL',
                    icon: (
                        <SiGraphql
                            size='1.4em'
                            color='deepPink'
                            role='presentation'
                        />
                    ),
                },
            ],
        },

        // Backend
        {
            name: 'Client',
            technologies: [
                {
                    name: 'React.js',
                    icon: (
                        <SiReact
                            size='1.4em'
                            color='deepSkyBlue'
                            role='presentation'
                        />
                    ),
                },
                {
                    name: 'Next.js',
                    icon: (
                        <SiNextdotjs
                            size='1.4em'
                            color='grey'
                            role='presentation'
                        />
                    ),
                },
                {
                    name: 'MUI',
                    icon: (
                        <SiMui
                            size='1.4em'
                            color='dodgerBlue'
                            role='presentation'
                        />
                    ),
                },
            ],
        },

        // Frontend
        {
            name: 'Serveur',
            technologies: [
                {
                    name: 'Node.js',
                    icon: (
                        <SiNodedotjs
                            size='1.4em'
                            color='yellowGreen'
                            role='presentation'
                        />
                    ),
                },
                {
                    name: 'Fastify',
                    icon: (
                        <SiFastify
                            size='1.4em'
                            color='white'
                            role='presentation'
                        />
                    ),
                },
                {
                    name: 'Prisma',
                    icon: (
                        <SiPrisma
                            size='1.4em'
                            color='steelBlue'
                            role='presentation'
                        />
                    ),
                },
            ],
        },

        // Tests
        {
            name: 'Tests',
            technologies: [
                {
                    name: 'Playwright',
                    icon: (
                        <SiPlaywright
                            size='1.4em'
                            color='green'
                            role='presentation'
                        />
                    ),
                },
                {
                    name: 'Jest',
                    icon: (
                        <SiJest
                            size='1.4em'
                            color='paleVioletRed'
                            role='presentation'
                        />
                    ),
                },
                {
                    name: 'Testing Library',
                    icon: (
                        <SiTestinglibrary
                            size='1.4em'
                            color='crimson'
                            role='presentation'
                        />
                    ),
                },
            ],
        },
    ]

    return (
        <>
            <Heading>{'💪 Compétences'}</Heading>

            <div className={styles.skills}>
                {skills.map((category) => (
                    <Card
                        key={category.name}
                        className={styles.technoGroup}
                    >
                        {/* Category name */}
                        <Heading variant='h2'>{category.name}</Heading>

                        {/* Technologies */}
                        <List>
                            {category.technologies.map((technology) => (
                                <ListItem
                                    key={technology.name}
                                    className={styles.techno}
                                >
                                    {technology.icon}
                                    {technology.name}
                                </ListItem>
                            ))}
                        </List>
                    </Card>
                ))}
            </div>
        </>
    )
}
