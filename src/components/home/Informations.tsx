import type { JSX } from 'react'

import { Card, Heading, Link } from '~/ui'

import { links } from '~/utils/constants'

import * as styles from './Informations.css'

export const Informations = (): JSX.Element => {
    return (
        <>
            <Heading>{'ℹ️ Informations'}</Heading>

            <div className={styles.content}>
                {/* Statut */}
                <Card>
                    <Heading variant='h2'>{'Statut :'}</Heading>

                    <div className={styles.text}>
                        {'Lead Developer Frontend chez '}
                        <Link href={links.various.COMPANY}>{'e-thik'}</Link>
                        {'.'}
                    </div>
                </Card>

                {/* Statut */}
                <Card>
                    <Heading variant='h2'>{'Me contacter :'}</Heading>

                    <div className={styles.text}>
                        {'Pour me contacter, voir mon LinkedIn.'}
                    </div>
                </Card>
            </div>
        </>
    )
}
