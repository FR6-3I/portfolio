import { style } from '@vanilla-extract/css'

import { BP_LG } from '~/theme/theme.css'

export const content = style({
    display: 'grid',
    marginBottom: '5vh',
    '@media': {
        [BP_LG]: { gridTemplateColumns: 'repeat(2, 1fr)' },
    },
})

export const text = style({
    fontSize: '1.2em',
})
