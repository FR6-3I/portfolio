import { style } from '@vanilla-extract/css'

export const text = style({
    marginBottom: '50px',
    textIndent: '50px',
    textAlign: 'justify',
})
