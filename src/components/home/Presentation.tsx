import type { JSX } from 'react'

import { Card, Heading } from '~/ui'

import * as styles from './Presentation.css'

export const Presentation = (): JSX.Element => {
    return (
        <>
            <Heading>{'✋ Présentation'}</Heading>

            <Card
                className={styles.text}
                variant='pressed'
            >
                {`Passionné par l’informatique, les nouvelles technologies, le logiciel libre et l’open-source. 🤓
          L’essentiel de mes compétences provient de ma capacité à être autonome, autodidacte. 🧐
          Mon aptitude à traiter l’information, la synthétiser pour une orientation à long terme dont je fais preuve,
          sont des atouts qui ne sont pas des moindres dans ce domaine. 😎
          J’ai également rejoint le culte de la rouille ! 🦀`}
            </Card>
        </>
    )
}
