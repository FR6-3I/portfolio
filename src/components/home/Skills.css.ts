import { globalStyle, style } from '@vanilla-extract/css'

import { BP_2XL, BP_SM } from '~/theme/theme.css'

export const skills = style({
    display: 'grid',
    '@media': {
        [BP_SM]: { gridTemplateColumns: 'repeat(2, 1fr)' },
        [BP_2XL]: { gridTemplateColumns: 'repeat(4, 1fr)' },
    },
})

export const technoGroup = style({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
})

export const techno = style({
    display: 'flex',
    alignItems: 'center',
})

globalStyle(`${techno} > svg`, {
    marginRight: '10px',
})
