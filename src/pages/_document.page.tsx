import type { JSX } from 'react'
import { Head, Html, Main, NextScript } from 'next/document'

import { SITE_NAME } from '~/utils/constants'
import { theme } from '~/theme/theme.css'

const MyDocument = (): JSX.Element => {
    return (
        <Html lang='fr'>
            <Head>
                <meta
                    name='description'
                    content={SITE_NAME}
                />
                <meta
                    name='theme-color'
                    content={theme.palette.primary.main}
                />

                <link
                    rel='manifest'
                    href='/manifest.json'
                />
                <link
                    rel='icon'
                    href='/icons/icon-16x16.png'
                    type='image/png'
                    sizes='16x16'
                />
                <link
                    rel='icon'
                    href='/icons/icon-32x32.png'
                    type='image/png'
                    sizes='32x32'
                />
                <link
                    rel='icon'
                    href='/icons/icon-48x48.png'
                    type='image/png'
                    sizes='48x48'
                />
                <link
                    rel='apple-touch-icon'
                    href='/icons/icon-180x180.png'
                    sizes='180x180'
                />
            </Head>

            <body>
                <Main />
                <NextScript />
            </body>
        </Html>
    )
}

export default MyDocument
