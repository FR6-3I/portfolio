import type { JSX } from 'react'
import type { AppProps } from 'next/app'
import { ThemeProvider } from 'next-themes'
import '@fontsource/figtree'

import { Link } from '~/ui'
import Footer from '~/components/app/footer'
import Header from '~/components/app/header'

import dark from '~/theme/dark.css'
import light from '~/theme/light.css'

import * as styles from './_app.css'

const MyApp = ({ Component, pageProps }: AppProps): JSX.Element => {
    return (
        <ThemeProvider
            attribute='class'
            value={{ dark, light }}
        >
            <div className={styles.root}>
                <Link
                    className={styles.goToMain}
                    href='#main'
                >
                    {'Passer au contenu principal'}
                </Link>

                <Header />

                <main
                    id='main'
                    className={styles.main}
                >
                    <Component {...pageProps} />
                </main>

                <Footer />
            </div>
        </ThemeProvider>
    )
}

export default MyApp
