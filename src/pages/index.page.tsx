import type { JSX } from 'react'
import Head from 'next/head'
import Image from 'next/image'

import { Heading, Link } from '~/ui'
import { Informations, Presentation, Skills } from '~/components/home'

import { SITE_NAME, urls } from '~/utils/constants'
import { BP_MD } from '~/theme/theme.css'

import profile from '~/../public/profile.jpg'
import * as styles from './index.css'

const Index = (): JSX.Element => {
    return (
        <>
            <Head>
                <title>{SITE_NAME}</title>
            </Head>

            <>
                <div className={styles.imageAndMe}>
                    <div className={styles.imageBlock}>
                        <Image
                            src={profile}
                            alt='Profil'
                            quality={100}
                            sizes={`100vw, ${BP_MD} 40vw`}
                            fill
                            priority
                        />
                    </div>

                    <Heading
                        className={styles.me}
                        center
                    >
                        <div>{'THÉRY Francis'}</div>
                        <div>{'Développeur fullstack'}</div>
                    </Heading>
                </div>

                <Informations />

                <Presentation />

                <Skills />

                {/* Redirection vers mes projets */}
                <Heading
                    variant='h2'
                    center
                >
                    {'Pour en découvrir davantage, explorez '}
                    <Link href={urls.PROJECTS}>{'mes projets'}</Link>
                    {'. 👀'}
                </Heading>
            </>
        </>
    )
}

export default Index
