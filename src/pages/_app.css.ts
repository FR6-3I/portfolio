import { style } from '@vanilla-extract/css'

import { BP_LG, BP_MD, BP_SM, BP_XL, theme } from '~/theme/theme.css'

export const root = style({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    minHeight: '100vh',
})

export const goToMain = style({
    position: 'absolute',
    zIndex: 10,
    top: '-50px',
    left: '0',
    padding: '10px',
    color: theme.palette.text.primary,
    backgroundColor: theme.palette.background.primary,
    ':focus': {
        top: '0',
    },
})

export const main = style({
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    width: '90%',
    marginTop: '5vh',
    marginBottom: '10vh',
    '@media': {
        [BP_SM]: { width: '85%' },
        [BP_MD]: { width: '75%' },
        [BP_LG]: { width: '65%' },
        [BP_XL]: { width: '55%' },
    },
})
