import { style } from '@vanilla-extract/css'

import { BP_2XL, BP_LG, BP_MD, BP_SM, BP_XL, theme } from '~/theme/theme.css'

export const imageAndMe = style({
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '2vh',
    '@media': {
        [BP_MD]: { flexDirection: 'unset' },
    },
})

export const imageBlock = style({
    zIndex: -1,
    position: 'relative',
    width: '50vw',
    height: '50vw',
    clipPath: 'circle()',
    '@media': {
        [BP_SM]: {
            width: '30vw',
            height: '30vw',
        },
        [BP_MD]: {
            width: '20vw',
            height: '20vw',
            marginRight: '2vw',
        },
        [BP_LG]: {
            width: '15vw',
            height: '15vw',
        },
        [BP_XL]: {
            width: '12vw',
            height: '12vw',
        },
        [BP_2XL]: {
            width: '10vw',
            height: '10vw',
            marginRight: '5vw',
        },
    },
})

export const me = style({
    fontSize: '3em',
    fontStyle: 'italic',
    color: theme.palette.primary.main,
})
