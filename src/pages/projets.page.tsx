import type { JSX } from 'react'
import Head from 'next/head'

import { Heading, Link } from '~/ui'
import {
    Introduction,
    Projects as ProjectsComponent,
} from '~/components/projects'

import { links, SITE_NAME } from '~/utils/constants'

import type { Project } from '~/types/general'

const Projects = (): JSX.Element => {
    const projects: Project[] = [
        {
            name: 'Poké-Infos',
            linkOnline: links.online.POKE_INFOS,
            linkCode: links.code.POKE_INFOS,
            description: (
                <>
                    {
                        'Poké-Infos est une sorte de Pokédex basique en ligne. Il s’appuie sur deux API, l’une pour les données ('
                    }

                    <Link href={links.various.POKEAPI}>{'pokeapi.co'}</Link>

                    {') et l’autre, pour les images ('}

                    <Link href={links.various.POKEMON}>{'pokemon.com'}</Link>

                    {
                        '). Il est disponible en trois langues (français 🇫🇷, anglais 🇬🇧 et allemand 🇩🇪) et utilise trois thèmes (classique, pokéball et ectoplasma).'
                    }
                </>
            ),
        },
    ]

    return (
        <>
            <Head>
                <title>{`Projets - ${SITE_NAME}`}</title>
            </Head>

            <>
                <Introduction />

                <ProjectsComponent projects={projects} />

                {/* Développement en cours */}
                <Heading
                    variant='h2'
                    center
                >
                    {'Un autre projet est en cours de développement. 🚧'}
                </Heading>
            </>
        </>
    )
}

export default Projects
