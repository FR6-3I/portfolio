import { style } from '@vanilla-extract/css'

export const root = style({
    marginTop: '10vh',
    textAlign: 'center',
    fontSize: '1.5em',
})
