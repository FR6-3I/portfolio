import type { JSX } from 'react'
import Head from 'next/head'

import { Heading } from '~/ui'

import { SITE_NAME } from '~/utils/constants'

import * as styles from './404.css'

const Error404 = (): JSX.Element => {
    return (
        <>
            <Head>
                <title>{`Erreur 404 - ${SITE_NAME}`}</title>
            </Head>

            <div className={styles.root}>
                <Heading center>{'Erreur 404'}</Heading>

                <div>
                    {
                        'Une erreur est survenue, veuillez vous rediriger vers l’accueil.'
                    }
                </div>
            </div>
        </>
    )
}

export default Error404
