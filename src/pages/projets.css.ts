import { style } from '@vanilla-extract/css'

export const inProgress = style({
    marginTop: '10vh',
    marginBottom: '5vh',
    textAlign: 'center',
    fontSize: '1.5em',
    fontWeight: 'bold',
})
