/* eslint-disable @typescript-eslint/no-redeclare */
import type { ReactElement } from 'react'
import { isValidElement } from 'react'
import { z } from 'zod'

/** React Element used for Zod. */
const ZodReactElement = z.custom<ReactElement>(
    (element) => isValidElement(element),
    'Value must be a React Element.',
)
type ZodReactElement = z.infer<typeof ZodReactElement>

/** Themes available in the application. */
export const Theme = z.enum(['dark', 'light'])
export type Theme = z.infer<typeof Theme>

/** Url to navigate inside the app. */
export const UrlHeader = z.object({
    name: z.string(),
    tooltip: z.string(),
    href: z.string(),
    icon: ZodReactElement,
})

export type UrlHeader = z.infer<typeof UrlHeader>

/** Information that a project contains. */
export const Project = z.object({
    name: z.string(),
    linkOnline: z.string(),
    linkCode: z.string(),
    description: ZodReactElement,
})

export type Project = z.infer<typeof Project>

/** Technology name and its logo. */
const Technology = z.object({
    name: z.string(),
    icon: ZodReactElement,
})

type Technology = z.infer<typeof Technology>

/** Skill name and technologies associated. */
export const Skill = z.object({
    name: z.string(),
    technologies: Technology.array(),
})

export type Skill = z.infer<typeof Skill>

/** Link object in the cards footer. */
export const LinkFooter = z.object({
    name: z.string().min(1),
    link: z.string().min(1),
})

export type LinkFooter = z.infer<typeof LinkFooter>
