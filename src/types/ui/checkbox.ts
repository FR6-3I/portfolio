/* eslint-disable @typescript-eslint/no-redeclare */
import { z } from 'zod'

/** Style of checkbox. */
export const VariantCheckbox = z.enum(['button', 'checkbox'])
export type VariantCheckbox = z.infer<typeof VariantCheckbox>
