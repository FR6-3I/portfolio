/* eslint-disable @typescript-eslint/no-redeclare */
import { z } from 'zod'

/** Style of link. */
export const VariantLink = z.enum(['button', 'link'])
export type VariantLink = z.infer<typeof VariantLink>
