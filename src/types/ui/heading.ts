/* eslint-disable @typescript-eslint/no-redeclare */
import { z } from 'zod'

/** Style of heading. */
export const VariantHeading = z.enum(['h1', 'h2', 'h3'])
export type VariantHeading = z.infer<typeof VariantHeading>
