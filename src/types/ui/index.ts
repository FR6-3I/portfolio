/* eslint-disable @typescript-eslint/no-redeclare */
import { z } from 'zod'

/** Theme color possible. */
export const Color = z.enum(['primary', 'secondary'])
export type Color = z.infer<typeof Color>
