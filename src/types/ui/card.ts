/* eslint-disable @typescript-eslint/no-redeclare */
import { z } from 'zod'

/** Style of card. */
export const VariantCard = z.enum(['flat', 'pressed'])
export type VariantCard = z.infer<typeof VariantCard>
