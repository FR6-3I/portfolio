export const SITE_NAME = 'Portfolio de Francis THÉRY, développeur fullstack'

export const urls = {
    HOMEPAGE: '/',
    ERROR_404: '/404',
    PROJECTS: '/projets',
} as const

export const links = {
    code: {
        POKE_INFOS: 'https://gitlab.com/FR6-3I/poke-infos',
        PORTFOLIO: 'https://gitlab.com/FR6-3I/portfolio',
    },

    online: {
        POKE_INFOS: 'https://poke-infos.francis-thery.dev',
        PORTFOLIO: 'https://francis-thery.dev',
    },

    profile: {
        GITLAB: 'https://gitlab.com/FR6-3I',
        LINKEDIN: 'https://www.linkedin.com/in/francis-thery',
    },

    various: {
        COMPANY: 'https://www.e-thik.com',
        ETNA: 'https://etna.io',
        ETNA_DIPLOME:
            'https://etna.io/formation/master-of-science-1-2-developpeur-dapplication-et-architecte-logiciel',
        POKEAPI: 'https://pokeapi.co',
        POKEMON: 'https://www.pokemon.com/fr',
    },
} as const
