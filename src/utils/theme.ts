/**
 * Merge multiple CSS class names in one.
 * @param styles List of CSS class names with potential conditional values.
 * @returns Class name filtered of bad values and merged in one string.
 */
export const joinStyles = (
    ...styles: (string | false | undefined)[]
): string => {
    return styles.filter(Boolean).join(' ')
}
