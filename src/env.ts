/* eslint-disable unicorn/prevent-abbreviations */
import { createEnv } from '@t3-oss/env-nextjs'
import { z } from 'zod'

export const env = createEnv({
    client: {
        NEXT_PUBLIC_ADDRESS: z.string().url(),
    },

    server: {
        PORT: z.coerce.number(),
    },

    runtimeEnv: {
        PORT: process.env.PORT,
        NEXT_PUBLIC_ADDRESS: process.env.NEXT_PUBLIC_ADDRESS,
    },
})
