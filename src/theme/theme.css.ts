import { createGlobalTheme, globalStyle } from '@vanilla-extract/css'

import { themeContract } from './contracts.css'

export const BP_SM = '(min-width: 640px)'
export const BP_MD = '(min-width: 768px)'
export const BP_LG = '(min-width: 1024px)'
export const BP_XL = '(min-width: 1280px)'
export const BP_2XL = '(min-width: 1536px)'

const global = createGlobalTheme(':root', {
    margin: '20px',
    padding: '15px',
    borderRadius: '10px',
    transition: '0.2s',
})

export const theme = {
    ...global,
    ...themeContract,
}

globalStyle('*', {
    boxSizing: 'border-box',
    transition: theme.transition,
})

globalStyle('body', {
    margin: '0',
    fontFamily: 'Figtree',
    color: theme.palette.text.primary,
    backgroundColor: theme.palette.background.primary,
})
