import { createTheme } from '@vanilla-extract/css'

import { themeContract } from './contracts.css'

const light = createTheme(themeContract, {
    palette: {
        primary: {
            light: 'hsl(0, 80%, 45%)',
            main: 'hsl(0, 100%, 30%)',
            dark: 'hsl(0, 90%, 25%)',
        },
        text: {
            primary: 'hsl(0, 0%, 10%)',
            secondary: 'hsl(0, 0%, 0%)',
        },
        background: {
            primary: 'hsl(0, 0%, 90%)',
            secondary: 'hsl(0, 0%, 80%)',
        },
    },

    boxShadow: {
        flat: {
            primary: `
        -10px -10px 10px hsl(0, 0%, 95%),
        10px 10px 10px hsl(0, 0%, 80%)
      `,
            secondary: `
        -10px -10px 10px hsl(0, 0%, 85%),
        10px 10px 10px hsl(0, 0%, 65%)
      `,
        },

        pressed: {
            primary: `
        inset -10px -10px 5px hsl(0, 0%, 95%),
        inset 10px 10px 5px hsl(0, 0%, 80%)
      `,
            secondary: `
        inset -10px -10px 5px hsl(0, 0%, 85%),
        inset 10px 10px 5px hsl(0, 0%, 65%)
      `,
        },

        halfPressed: {
            primary: `
        -5px -5px 5px hsl(0, 0%, 95%),
        5px 5px 5px hsl(0, 0%, 80%)
      `,
            secondary: `
        -5px -5px 5px hsl(0, 0%, 85%),
        5px 5px 5px hsl(0, 0%, 65%)
      `,
        },
    },
})

export default light
