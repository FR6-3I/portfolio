/* eslint-disable unicorn/no-null */
import { createThemeContract } from '@vanilla-extract/css'

const paletteContract = createThemeContract({
    primary: {
        light: null,
        main: null,
        dark: null,
    },
    text: {
        primary: null,
        secondary: null,
    },
    background: {
        primary: null,
        secondary: null,
    },
})

const boxShadowContract = createThemeContract({
    flat: {
        primary: null,
        secondary: null,
    },
    pressed: {
        primary: null,
        secondary: null,
    },
    halfPressed: {
        primary: null,
        secondary: null,
    },
})

export const themeContract = createThemeContract({
    palette: paletteContract,
    boxShadow: boxShadowContract,
})
