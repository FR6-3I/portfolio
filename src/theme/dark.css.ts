import { createTheme } from '@vanilla-extract/css'

import { themeContract } from './contracts.css'

const dark = createTheme(themeContract, {
    palette: {
        primary: {
            light: 'hsl(0, 80%, 75%)',
            main: 'hsl(0, 80%, 65%)',
            dark: 'hsl(0, 80%, 35%)',
        },
        text: {
            primary: 'hsl(0, 0%, 80%)',
            secondary: 'hsl(0, 0%, 90%)',
        },
        background: {
            primary: 'hsl(220, 20%, 8%)',
            secondary: 'hsl(220, 20%, 15%)',
        },
    },

    boxShadow: {
        flat: {
            primary: `
        -10px -10px 10px hsl(220, 20%, 9%),
        10px 10px 10px hsl(220, 20%, 6%)
      `,
            secondary: `
        -10px -10px 10px hsl(220, 20%, 17%),
        10px 10px 10px hsl(220, 20%, 12%)
      `,
        },

        pressed: {
            primary: `
        inset -10px -10px 5px hsl(220, 20%, 9%),
        inset 10px 10px 5px hsl(220, 20%, 6%)
      `,
            secondary: `
        inset -10px -10px 5px hsl(220, 20%, 17%),
        inset 10px 10px 5px hsl(220, 20%, 12%)
      `,
        },

        halfPressed: {
            primary: `
        -5px -5px 5px hsl(220, 20%, 9%),
        5px 5px 5px hsl(220, 20%, 6%)
      `,
            secondary: `
        -5px -5px 5px hsl(220, 20%, 17%),
        5px 5px 5px hsl(220, 20%, 12%)
      `,
        },
    },
})

export default dark
