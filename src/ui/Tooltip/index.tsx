/* eslint no-inline-comments: 'off' */
import type { ComponentProps, JSX } from 'react'

import { joinStyles } from '~/utils/theme'

import * as styles from './index.css'

type TooltipProperties = ComponentProps<'div'> & {
    text: string
}

export const Tooltip = ({
    children,
    text,
    ...properties
}: Readonly<TooltipProperties>): JSX.Element => {
    return (
        <div
            className={joinStyles(
                styles.root,
                /* @ts-expect-error: props is type of any */ /* eslint-disable-next-line @typescript-eslint/no-unsafe-member-access */
                children.props.className as string | undefined,
            )}
            {...properties}
        >
            {children}

            <div className={styles.tooltip}>{text}</div>
        </div>
    )
}
