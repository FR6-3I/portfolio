import { globalStyle, style } from '@vanilla-extract/css'

import { theme } from '~/theme/theme.css'

export const root = style({
    position: 'relative',
})

export const tooltip = style({
    zIndex: 10,
    position: 'absolute',
    top: '120%',
    left: '50%',
    transform: 'translateX(-50%)',
    display: 'none',
    width: '100%',
    padding: '10px',
    textAlign: 'center',
    fontSize: '0.8em',
    borderRadius: theme.borderRadius,
    backgroundColor: theme.palette.background.secondary,
    boxShadow: theme.boxShadow.halfPressed.secondary,
})

globalStyle(`${root}:hover > ${tooltip}`, {
    display: 'block',
})
