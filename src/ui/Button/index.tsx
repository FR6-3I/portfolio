import type { ComponentProps, JSX } from 'react'

import { joinStyles } from '~/utils/theme'

import type { Color } from '~/types/ui'
import * as styles from './index.css'

type ButtonProperties = ComponentProps<'button'> & {
    color?: Color
}

export const Button = ({
    color = 'primary',
    className,
    ...properties
}: Readonly<ButtonProperties>): JSX.Element => {
    return (
        <button
            className={joinStyles(styles[color], className)}
            {...properties}
        />
    )
}
