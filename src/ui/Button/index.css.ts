import { style } from '@vanilla-extract/css'

import { theme } from '~/theme/theme.css'

const base = style({
    cursor: 'pointer',
    display: 'inline-flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: theme.margin,
    padding: '10px 20px',
    fontSize: '1.2em',
    fontWeight: 'bold',
    border: 'none',
    color: theme.palette.primary.main,
    borderRadius: theme.borderRadius,
    ':hover': {
        color: theme.palette.primary.light,
    },
})

export const primary = style([
    base,
    {
        backgroundColor: theme.palette.background.primary,
        boxShadow: theme.boxShadow.flat.primary,
        ':hover': {
            boxShadow: theme.boxShadow.halfPressed.primary,
        },
        ':active': {
            boxShadow: theme.boxShadow.pressed.primary,
        },
    },
])

export const secondary = style([
    base,
    {
        backgroundColor: theme.palette.background.secondary,
        boxShadow: theme.boxShadow.flat.secondary,
        ':hover': {
            boxShadow: theme.boxShadow.halfPressed.secondary,
        },
        ':active': {
            boxShadow: theme.boxShadow.pressed.secondary,
        },
    },
])
