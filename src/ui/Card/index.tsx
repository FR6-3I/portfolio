import type { ComponentProps, JSX } from 'react'

import { joinStyles } from '~/utils/theme'

import type { Color } from '~/types/ui'
import type { VariantCard } from '~/types/ui/card'
import * as styles from './index.css'

type CardProperties = ComponentProps<'div'> & {
    variant?: VariantCard
    color?: Color
}

export const Card = ({
    className,
    variant = 'flat',
    color = 'primary',
    ...properties
}: Readonly<CardProperties>): JSX.Element => {
    return (
        <div
            className={joinStyles(styles[variant][color], className)}
            {...properties}
        />
    )
}
