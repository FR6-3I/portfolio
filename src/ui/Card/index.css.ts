import { style, styleVariants } from '@vanilla-extract/css'
import { calc } from '@vanilla-extract/css-utils'

import { theme } from '~/theme/theme.css'

const base = style({
    margin: theme.margin,
    padding: calc(theme.padding).multiply(2).toString(),
    borderRadius: theme.borderRadius,
})

/** @knipignore */
export const flat = styleVariants({
    primary: [
        base,
        {
            boxShadow: theme.boxShadow.flat.primary,
        },
    ],
    secondary: [
        base,
        {
            boxShadow: theme.boxShadow.flat.secondary,
        },
    ],
})

/** @knipignore */
export const pressed = styleVariants({
    primary: [
        base,
        {
            boxShadow: theme.boxShadow.pressed.primary,
        },
    ],
    secondary: [
        base,
        {
            boxShadow: theme.boxShadow.pressed.secondary,
        },
    ],
})
