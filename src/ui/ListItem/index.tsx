import type { ComponentProps, JSX } from 'react'

import { joinStyles } from '~/utils/theme'

import * as styles from './index.css'

type ListItemProperties = ComponentProps<'li'>

export const ListItem = ({
    children,
    className,
    ...properties
}: ListItemProperties): JSX.Element => {
    return (
        <li
            className={joinStyles(styles.root, className)}
            {...properties}
        >
            {children}
        </li>
    )
}
