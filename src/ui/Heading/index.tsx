import type { ComponentProps, JSX } from 'react'

import { joinStyles } from '~/utils/theme'

import type { VariantHeading } from '~/types/ui/heading'
import * as styles from './index.css'

type HeadingProperties = ComponentProps<'h1'> & {
    variant?: VariantHeading
    center?: boolean
}

export const Heading = ({
    children,
    className,
    variant = 'h1',
    center = false,
    ...properties
}: Readonly<HeadingProperties>): JSX.Element => {
    return (
        <>
            {/* H1 */}
            {variant === 'h1' && (
                <h1
                    className={joinStyles(center && styles.center, className)}
                    {...properties}
                >
                    {children}
                </h1>
            )}

            {/* H2 */}
            {variant === 'h2' && (
                <h2
                    className={joinStyles(center && styles.center, className)}
                    {...properties}
                >
                    {children}
                </h2>
            )}

            {/* H3 */}
            {variant === 'h3' && (
                <h3
                    className={joinStyles(center && styles.center, className)}
                    {...properties}
                >
                    {children}
                </h3>
            )}
        </>
    )
}
