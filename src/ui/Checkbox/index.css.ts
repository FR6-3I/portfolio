import { globalStyle, style, styleVariants } from '@vanilla-extract/css'

import {
    primary as buttonPrimary,
    secondary as buttonSecondary,
} from '~/ui/Button/index.css'

import { theme } from '~/theme/theme.css'

const base = style({
    cursor: 'pointer',
    display: 'flex',
    alignItems: 'center',
    width: 'fit-content',
    margin: '10px',
})

const baseButton = style({
    justifyContent: 'center',
    textAlign: 'center',
})

/** @knipignore */
export const checkbox = styleVariants({
    primary: [base],
    secondary: [base],
})

/** @knipignore */
export const button = styleVariants({
    primary: [baseButton, buttonPrimary],
    secondary: [baseButton, buttonSecondary],
})

export const input = style({
    cursor: 'pointer',
    margin: '0px',
    padding: theme.padding,
    accentColor: theme.palette.primary.main,
})

export const label = style({
    paddingLeft: theme.padding,
})

globalStyle(`${baseButton} > input`, {
    opacity: 0,
    position: 'absolute',
})

globalStyle(`${buttonPrimary}:has(> input:checked)`, {
    boxShadow: theme.boxShadow.pressed.primary,
})

globalStyle(`${buttonSecondary}:has(> input:checked)`, {
    boxShadow: theme.boxShadow.pressed.secondary,
})
