import type { ComponentProps, JSX } from 'react'

import { joinStyles } from '~/utils/theme'

import type { Color } from '~/types/ui'
import type { VariantCheckbox } from '~/types/ui/checkbox'
import * as styles from './index.css'

type CheckboxProperties = ComponentProps<'input'> & {
    variant?: VariantCheckbox
    color?: Color
    label?: string
}

export const Checkbox = ({
    children,
    className,
    variant = 'checkbox',
    color = 'primary',
    label,
    ...properties
}: Readonly<CheckboxProperties>): JSX.Element => {
    return (
        <label className={joinStyles(styles[variant][color], className)}>
            <input
                className={styles.input}
                type='checkbox'
                {...properties}
            />

            {children}

            {!!label && <div className={styles.label}>{label}</div>}
        </label>
    )
}
