import { style, styleVariants } from '@vanilla-extract/css'

import {
    primary as buttonPrimary,
    secondary as buttonSecondary,
} from '~/ui/Button/index.css'

import { theme } from '~/theme/theme.css'

const base = style({
    textDecoration: 'none',
})

const baseLink = style({
    color: theme.palette.primary.main,
    ':visited': {
        color: theme.palette.primary.main,
    },
    ':hover': {
        color: theme.palette.primary.light,
    },
})

/** @knipignore */
export const link = styleVariants({
    primary: [baseLink],
    secondary: [baseLink],
})

/** @knipignore */
export const button = styleVariants({
    primary: [base, buttonPrimary],
    secondary: [base, buttonSecondary],
})
