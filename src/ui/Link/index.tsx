import type { ComponentProps, JSX } from 'react'
import NextLink from 'next/link'

import { joinStyles } from '~/utils/theme'

import type { Color } from '~/types/ui'
import type { VariantLink } from '~/types/ui/link'
import * as styles from './index.css'

type LinkProperties = ComponentProps<'a'> & {
    variant?: VariantLink
    color?: Color
    href: string
    external?: boolean
}

export const Link = ({
    children,
    className,
    variant = 'link',
    color = 'primary',
    href,
    external = false,
    ...properties
}: Readonly<LinkProperties>): JSX.Element => {
    const isExternalLink = href.startsWith('http')

    return (
        <>
            {/* Lien interne */}
            {!isExternalLink && (
                /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
                /* @ts-expect-error */
                <NextLink
                    className={joinStyles(styles[variant][color], className)}
                    href={href}
                    {...(external && {
                        target: '_blank',
                        rel: 'noreferrer',
                    })}
                    {...properties}
                >
                    {children}
                </NextLink>
            )}

            {/* Lien externe */}
            {!!isExternalLink && (
                <a
                    className={joinStyles(styles[variant][color], className)}
                    href={href}
                    target='_blank'
                    rel='noreferrer'
                    {...properties}
                >
                    {children}
                </a>
            )}
        </>
    )
}
