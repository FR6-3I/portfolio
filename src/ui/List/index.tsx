import type { ComponentProps, JSX } from 'react'

import { joinStyles } from '~/utils/theme'

import * as styles from './index.css'

type ListProperties = ComponentProps<'ul'>

export const List = ({
    className,
    ...properties
}: ListProperties): JSX.Element => {
    return (
        <ul
            className={joinStyles(styles.root, className)}
            {...properties}
        />
    )
}
