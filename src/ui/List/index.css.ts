import { style } from '@vanilla-extract/css'

export const root = style({
    margin: '0px',
    padding: '0px',
    listStyleType: 'none',
})
