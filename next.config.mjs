import { fileURLToPath } from 'node:url'

import withPlugins from 'next-compose-plugins'
import { createVanillaExtractPlugin } from '@vanilla-extract/next-plugin'
import withBundleAnalyzer from '@next/bundle-analyzer'
import createJiti from 'jiti'

const jiti = createJiti(fileURLToPath(import.meta.url))
jiti('./src/env')

withBundleAnalyzer({
    enabled: process.env.ANALYZE === 'true',
})

const withVanillaExtract = createVanillaExtractPlugin()

/** @type {import('next').NextConfig} */
const nextConfig = {
    output: 'standalone',
    pageExtensions: ['page.tsx'],
    poweredByHeader: false,
    reactStrictMode: true,
}

const config = withPlugins([withBundleAnalyzer, withVanillaExtract, nextConfig])

export default config
