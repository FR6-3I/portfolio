/** @type {import("prettier").Options} */
const config = {
    jsxSingleQuote: true,
    semi: false,
    singleAttributePerLine: true,
    singleQuote: true,
    tabWidth: 4,
}

export default config
