module.exports = {
    root: true,
    env: {
        browser: true,
        es2022: true,
        node: true,
    },
    extends: [
        'eslint:all',
        'plugin:import/recommended',
        'plugin:import/typescript',
        'plugin:react/all',
        'plugin:react-hooks/recommended',
        'plugin:jsx-a11y/strict',
        'next/core-web-vitals',
        'plugin:promise/recommended',
        'plugin:@typescript-eslint/all',
        'plugin:unicorn/all',
        'plugin:deprecation/recommended',
        'plugin:jsdoc/recommended-typescript-error',
        'plugin:prettier/recommended',
    ],
    plugins: [
        '@typescript-eslint',
        'prefer-arrow-functions',
        'prettier',
        'simple-import-sort',
    ],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 'latest',
        project: true,
        sourceType: 'module',
        tsconfigRootDir: __dirname,
    },
    ignorePatterns: ['node_modules'],
    overrides: [
        {
            files: ['tests/**'],
            extends: ['plugin:playwright/playwright-test'],
        },
    ],
    rules: {
        'consistent-return': 'off',
        'max-lines-per-function': 'off',
        'no-duplicate-imports': 'off',
        'no-implicit-coercion': [
            'error',
            {
                allow: ['!!'],
                disallowTemplateShorthand: true,
            },
        ],
        'one-var': 'off',
        'sort-keys': 'off',
        'sort-imports': 'off',
        '@typescript-eslint/consistent-type-definitions': ['error', 'type'],
        '@typescript-eslint/naming-convention': 'off',
        '@typescript-eslint/no-magic-numbers': 'off',
        '@typescript-eslint/prefer-readonly-parameter-types': 'off',
        '@typescript-eslint/strict-boolean-expressions': 'off',
        'import/first': 'error',
        'import/namespace': ['error', { allowComputed: true }],
        'import/newline-after-import': [
            'error',
            {
                considerComments: true,
                exactCount: true,
            },
        ],
        'import/no-duplicates': 'error',
        'jsdoc/check-tag-names': ['error', { definedTags: ['knipignore'] }],
        'prefer-arrow-functions/prefer-arrow-functions': 'error',
        'react/button-has-type': 'off',
        'react/forbid-component-props': 'off',
        'react/jsx-curly-brace-presence': [
            'error',
            {
                children: 'always',
                propElementValues: 'always',
                props: 'never',
            },
        ],
        'react/jsx-filename-extension': ['error', { extensions: ['.tsx'] }],
        'react/function-component-definition': [
            'error',
            {
                namedComponents: 'arrow-function',
                unnamedComponents: 'arrow-function',
            },
        ],
        'react/jsx-max-depth': ['error', { max: 5 }],
        'react/jsx-no-bind': ['error', { allowArrowFunctions: true }],
        'react/jsx-props-no-spreading': 'off',
        'react/jsx-sort-props': 'off',
        'react/require-default-props': [
            'error',
            {
                functions: 'defaultArguments',
            },
        ],
        'simple-import-sort/imports': [
            'error',
            {
                groups: [
                    // Packages
                    [
                        '^react',
                        '^next',
                        '^@playwright',
                        '@axe-core',
                        '^next-themes',
                        '^react-icons',
                        '^@t3-oss',
                        'zod',
                        '^@vanilla-extract',
                        '^\\u0000',
                    ],

                    // UI
                    ['^~/ui', '^~/components', '^\\./'],

                    // Logic
                    ['^~/utils', '^~/theme'],

                    // Types, assets and style
                    ['^~/types', '^~/\\.\\./public', '^\\./.*\\.css$'],
                ],
            },
        ],
        'simple-import-sort/exports': 'error',
        'unicorn/filename-case': 'off',
        'unicorn/no-keyword-prefix': 'off',
        'unicorn/string-content': [
            'error',
            {
                patterns: {
                    "'": '’',
                    '\\.\\.\\.': '…',
                    '^http:\\/\\/': '^https:\\/\\/',
                },
            },
        ],
    },
}
