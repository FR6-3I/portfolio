import type { KnipConfig } from 'knip'

const knipConfiguration: KnipConfig = {
    ignore: ['next-sitemap.config.js', 'src/env.ts'],
    ignoreDependencies: [
        'eslint-import-resolver-typescript',
        '@t3-oss/env-nextjs',
        'sharp',
    ],
    rules: {
        classMembers: 'error',
        nsExports: 'error',
        nsTypes: 'error',
    },
}

export default knipConfiguration
