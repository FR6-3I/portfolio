import { devices, PlaywrightTestConfig } from '@playwright/test'

const config: PlaywrightTestConfig = {
    projects: [
        {
            name: 'Desktop Chromium',
            use: { ...devices['Desktop Chrome'] },
        },
        {
            name: 'Desktop Firefox',
            use: { ...devices['Desktop Firefox'] },
        },
    ],
    snapshotDir: './snapshots',
    retries: 1,
    timeout: 20_000,
    use: {
        baseURL: process.env.NEXT_PUBLIC_ADDRESS,
        trace: 'on-first-retry',
    },
    webServer: {
        command: 'pnpm dev',
        url: process.env.NEXT_PUBLIC_ADDRESS,
    },
}

export default config
