module.exports = {
    siteUrl: process.env.NEXT_PUBLIC_ADDRESS,
    changefreq: false,
    autoLastmod: false,
    generateRobotsTxt: true,
}
