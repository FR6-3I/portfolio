# Portfolio

## Description

&emsp;&emsp;&emsp;Cette application est un portofolio servant à me décrire.
Elle est adaptable selon l'écran (responsive).

&emsp;&emsp;&emsp;Ce projet utilise les technologies principales suivantes :

- Typescript
- Next.js
- Preact
- Jest
- Testing Library

## Télémétrie

&emsp;&emsp;&emsp;Si vous voulez désactiver la télémétrie (Next.js) de manière globale sur la machine, veuillez saisir la commande suivante:

```shell
# npm
npx next telemetry disable

# pnpm
pnpx next telemetry disable

# yarn
yarn next telemetry disable
```

## Mise en place

&emsp;&emsp;&emsp;Tout d'abord, si le but est uniquement d'avoir un aperçu, d'essayer l'application. Il est possible de le faire directement à [cette adresse](https://francis-thery.dev).

Télécharger l'application :

```shell
git clone https://gitlab.com/FR6-3I/portfolio.git

cd portfolio
```

Installer les dépendances du projet :

```shell
# npm
npm i

# pnpm
pnpm i

# yarn
yarn
```

Créer un fichier *.env.local* et y instancier les variables selon le schéma de *.env* si nécessaire.

Lancer l'application en mode production :

```shell
# npm
npm run build && npm run start

# pnpm
pnpm build && pnpm start

# yarn
yarn build && yarn start
```
