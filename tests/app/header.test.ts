import { expect, test } from '@playwright/test'

import { links, urls } from '~/utils/constants'

test.beforeEach(async ({ page }) => {
    await page.goto(urls.HOMEPAGE)
})

test.describe('header', () => {
    test('Homepage and projects buttons', async ({ page }) => {
        const accueil = page.getByRole('link', {
            name: 'Logo Portfolio',
            exact: true,
        })

        const projets = page.getByRole('link', {
            name: 'Projets',
            exact: true,
        })

        await expect(accueil).toHaveAttribute('href', urls.HOMEPAGE)
        await expect(projets).toHaveAttribute('href', urls.PROJECTS)
    })

    test('GitLab and LinkedIn profiles', async ({ page }) => {
        const gitlab = page.getByRole('link', {
            name: 'Profil GitLab',
            exact: true,
        })

        const linkedin = page.getByRole('link', {
            name: 'Profil LinkedIn',
            exact: true,
        })

        await expect(gitlab).toHaveAttribute('href', links.profile.GITLAB)
        await expect(gitlab).toHaveAttribute('target', '_blank')
        await expect(gitlab).toHaveAttribute('rel', 'noreferrer')

        await expect(linkedin).toHaveAttribute('href', links.profile.LINKEDIN)
        await expect(linkedin).toHaveAttribute('target', '_blank')
        await expect(linkedin).toHaveAttribute('rel', 'noreferrer')
    })

    test('bouton thème', async ({ page }) => {
        await expect(
            page.getByRole('button', {
                name: 'Sombre',
                exact: true,
            }),
        ).toBeVisible()
    })
})
