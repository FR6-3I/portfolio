import { expect, test } from '@playwright/test'

import { links, urls } from '~/utils/constants'

test.beforeEach(async ({ page }) => {
    await page.goto(urls.HOMEPAGE)
})

test.describe('footer', () => {
    test('en ligne', async ({ page }) => {
        await expect(
            page.getByText('En ligne', {
                exact: true,
            }),
        ).toBeVisible()

        const portfolio = page
            .getByRole('link', {
                name: 'Portfolio',
                exact: true,
            })
            .nth(0)

        const pokeInfos = page
            .getByRole('link', {
                name: 'Poké-Infos',
                exact: true,
            })
            .nth(0)

        await expect(portfolio).toHaveAttribute('href', links.online.PORTFOLIO)
        await expect(pokeInfos).toHaveAttribute('href', links.online.POKE_INFOS)
    })

    test('code source', async ({ page }) => {
        await expect(
            page.getByText('Code source', {
                exact: true,
            }),
        ).toBeVisible()

        const portfolio = page
            .getByRole('link', {
                name: 'Portfolio',
                exact: true,
            })
            .nth(1)

        const pokeInfos = page
            .getByRole('link', {
                name: 'Poké-Infos',
                exact: true,
            })
            .nth(1)

        await expect(portfolio).toHaveAttribute('href', links.code.PORTFOLIO)
        await expect(pokeInfos).toHaveAttribute('href', links.code.POKE_INFOS)
    })

    test('réseaux', async ({ page }) => {
        await expect(
            page.getByText('Réseaux', {
                exact: true,
            }),
        ).toBeVisible()

        await expect(
            page.getByRole('link', {
                name: 'GitLab',
                exact: true,
            }),
        ).toHaveAttribute('href', links.profile.GITLAB)

        await expect(
            page.getByRole('link', {
                name: 'LinkedIn',
                exact: true,
            }),
        ).toHaveAttribute('href', links.profile.LINKEDIN)
    })
})
