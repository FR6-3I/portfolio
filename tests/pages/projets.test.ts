import { expect, test } from '@playwright/test'
import AxeBuilder from '@axe-core/playwright'

import { links, SITE_NAME, urls } from '~/utils/constants'

test.beforeEach(async ({ page }) => {
    await page.goto(urls.PROJECTS)
})

test.describe('page des projets', () => {
    test('général', async ({ page }) => {
        await expect(page).toHaveTitle(`Projets - ${SITE_NAME}`)

        expect(
            await page.screenshot({
                animations: 'disabled',
                fullPage: true,
            }),
        ).toMatchSnapshot('projets_light.png')

        const a11yLight = await new AxeBuilder({ page }).analyze()

        expect(a11yLight.violations).toHaveLength(0)

        await page
            .getByRole('button', {
                name: 'Sombre',
                exact: true,
            })
            .click()

        expect(
            await page.screenshot({
                animations: 'disabled',
                fullPage: true,
            }),
        ).toMatchSnapshot('projets_dark.png')

        const a11yDark = await new AxeBuilder({ page }).analyze()

        expect(a11yDark.violations).toHaveLength(0)
    })

    test('introduction', async ({ page }) => {
        await expect(
            page.getByRole('heading', {
                level: 1,
                name: '📖 Introduction',
                exact: true,
            }),
        ).toBeVisible()

        await expect(page.getByText('y compris ce portfolio')).toBeVisible()
    })

    test.describe('projets', () => {
        test('titre', async ({ page }) => {
            await expect(
                page.getByRole('heading', {
                    level: 1,
                    name: '📚 Projets',
                    exact: true,
                }),
            ).toBeVisible()
        })

        test('poké-infos', async ({ page }) => {
            await expect(
                page.getByRole('heading', {
                    level: 2,
                    name: 'Poké-Infos',
                    exact: true,
                }),
            ).toBeVisible()

            await expect(
                page.getByText('une sorte de Pokédex basique en ligne'),
            ).toBeVisible()

            await expect(
                page.getByRole('link', {
                    name: 'pokeapi.co',
                    exact: true,
                }),
            ).toHaveAttribute('href', links.various.POKEAPI)

            await expect(
                page.getByRole('link', {
                    name: 'En ligne',
                    exact: true,
                }),
            ).toHaveAttribute('href', links.online.POKE_INFOS)

            await expect(
                page.getByRole('link', {
                    name: 'Code source',
                    exact: true,
                }),
            ).toHaveAttribute('href', links.code.POKE_INFOS)
        })
    })

    test('en développement', async ({ page }) => {
        await expect(
            page.getByText(
                'Un autre projet est en cours de développement. 🚧',
                {
                    exact: true,
                },
            ),
        ).toBeVisible()
    })
})
