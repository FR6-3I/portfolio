import { expect, test } from '@playwright/test'
import AxeBuilder from '@axe-core/playwright'

import { SITE_NAME, urls } from '~/utils/constants'

test.beforeEach(async ({ page }) => {
    await page.goto(urls.ERROR_404)
})

test.describe('Page erreur 404', () => {
    test('général', async ({ page }) => {
        await expect(page).toHaveTitle(`Erreur 404 - ${SITE_NAME}`)

        expect(
            await page.screenshot({
                animations: 'disabled',
                fullPage: true,
            }),
        ).toMatchSnapshot('404_light.png')

        const a11yLight = await new AxeBuilder({ page }).analyze()

        expect(a11yLight.violations).toHaveLength(0)

        await page
            .getByRole('button', {
                name: 'Sombre',
                exact: true,
            })
            .click()

        expect(
            await page.screenshot({
                animations: 'disabled',
                fullPage: true,
            }),
        ).toMatchSnapshot('404.png')

        const a11yDark = await new AxeBuilder({ page }).analyze()

        expect(a11yDark.violations).toHaveLength(0)
    })

    test('contenu', async ({ page }) => {
        await expect(
            page.getByRole('heading', {
                level: 1,
                name: 'Erreur 404',
                exact: true,
            }),
        ).toBeVisible()

        await expect(
            page.getByText(
                'Une erreur est survenue, veuillez vous rediriger vers l’accueil.',
                {
                    exact: true,
                },
            ),
        ).toBeVisible()
    })
})
