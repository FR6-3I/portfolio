import { expect, test } from '@playwright/test'
import AxeBuilder from '@axe-core/playwright'

import { links, SITE_NAME, urls } from '~/utils/constants'

test.beforeEach(async ({ page }) => {
    await page.goto(urls.HOMEPAGE)
})

test.describe('page d’accueil', () => {
    test('général', async ({ page }) => {
        await expect(page).toHaveTitle(SITE_NAME)

        expect(
            await page.screenshot({
                animations: 'disabled',
                fullPage: true,
            }),
        ).toMatchSnapshot('index_light.png')

        const a11yLight = await new AxeBuilder({ page }).analyze()

        expect(a11yLight.violations).toHaveLength(0)

        await page
            .getByRole('button', {
                name: 'Sombre',
                exact: true,
            })
            .click()

        expect(
            await page.screenshot({
                animations: 'disabled',
                fullPage: true,
            }),
        ).toMatchSnapshot('index_dark.png')

        const a11yDark = await new AxeBuilder({ page }).analyze()

        expect(a11yDark.violations).toHaveLength(0)
    })

    test('introduction', async ({ page }) => {
        await expect(page.getByRole('img', { name: 'Profil' })).toBeVisible()

        await expect(
            page.getByRole('heading', {
                level: 1,
                name: 'THÉRY Francis Développeur fullstack',
                exact: true,
            }),
        ).toBeVisible()
    })

    test.describe('information', () => {
        test('title', async ({ page }) => {
            await expect(
                page.getByRole('heading', {
                    level: 1,
                    name: 'ℹ️ Informations',
                    exact: true,
                }),
            ).toBeVisible()
        })

        test('status', async ({ page }) => {
            await expect(
                page.getByRole('heading', {
                    level: 2,
                    name: 'Statut :',
                    exact: true,
                }),
            ).toBeVisible()

            await expect(
                page.getByRole('link', {
                    name: 'e-thik',
                    exact: true,
                }),
            ).toHaveAttribute('href', links.various.COMPANY)
        })

        test('contact me', async ({ page }) => {
            await expect(
                page.getByRole('heading', {
                    level: 2,
                    name: 'Me contacter :',
                    exact: true,
                }),
            ).toBeVisible()

            await expect(
                page.getByText('Pour me contacter, voir mon LinkedIn.', {
                    exact: true,
                }),
            ).toBeVisible()
        })
    })

    test('présentation', async ({ page }) => {
        await expect(
            page.getByRole('heading', {
                level: 1,
                name: '✋ Présentation',
                exact: true,
            }),
        ).toBeVisible()

        await expect(
            page.getByText('Passionné par l’informatique'),
        ).toBeVisible()
    })

    test.describe('compétences', () => {
        test('titre', async ({ page }) => {
            await expect(
                page.getByRole('heading', {
                    level: 1,
                    name: '💪 Compétences',
                    exact: true,
                }),
            ).toBeVisible()
        })

        test('langages', async ({ page }) => {
            await expect(
                page.getByRole('heading', {
                    level: 2,
                    name: 'Langages',
                    exact: true,
                }),
            ).toBeVisible()

            await expect(
                page.locator('h2:text("Langages") + ul > li'),
            ).toHaveText(['JavaScript', 'TypeScript', 'Rust', 'GraphQL'])
        })

        test('client', async ({ page }) => {
            await expect(
                page.getByRole('heading', {
                    level: 2,
                    name: 'Client',
                    exact: true,
                }),
            ).toBeVisible()

            await expect(
                page.locator('h2:text("Client") + ul > li'),
            ).toHaveText(['React.js', 'Next.js', 'MUI'])
        })

        test('serveur', async ({ page }) => {
            await expect(
                page.getByRole('heading', {
                    level: 2,
                    name: 'Serveur',
                    exact: true,
                }),
            ).toBeVisible()

            await expect(
                page.locator('h2:text("Serveur") + ul > li'),
            ).toHaveText(['Node.js', 'Fastify', 'Prisma'])
        })

        test('tests', async ({ page }) => {
            await expect(
                page.getByRole('heading', {
                    level: 2,
                    name: 'Tests',
                    exact: true,
                }),
            ).toBeVisible()

            await expect(page.locator('h2:text("Tests") + ul > li')).toHaveText(
                ['Playwright', 'Jest', 'Testing Library'],
            )
        })
    })

    test('découvrir davantage', async ({ page }) => {
        await expect(
            page.getByRole('heading', {
                level: 2,
                name: 'Pour en découvrir davantage, explorez mes projets. 👀',
                exact: true,
            }),
        ).toBeVisible()

        const projets = page.getByRole('link', {
            name: 'mes projets',
            exact: true,
        })

        await expect(projets).toHaveAttribute('href', urls.PROJECTS)
    })
})
